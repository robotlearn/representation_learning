import exputils as eu
import numpy as np
from torch.utils.data import Dataset
from torch.utils.data import DataLoader


class Grid:
    """
    Environment with a 28x28 grid where the agent is a point and can move up, down, left or right.
    The goal is to reach the top-right corner of the grid.
    """
    @staticmethod
    def default_config():
        dc = eu.AttrDict(
            start_pos=(14, 14),
            left_reward=0,
            right_reward=1,
            movement_punishment=-0.1,
            goal_positions=[(27, 27)],
        )
        return dc

    def __init__(self, config=None, **kwargs):
        self.config = eu.combine_dicts(kwargs, config, self.default_config())

        self.start_pos = self.config.start_pos
        self.left_reward = self.config.left_reward
        self.right_reward = self.config.right_reward
        self.movement_punishment = self.config.movement_punishment
        self.goal_positions = self.config.goal_positions

        # Initialize agent position
        self.agent_pos = None
        self.reset()

    def reset(self):
        # Start in the middle of the grid
        self.agent_pos = self.start_pos
        state = self.agent_pos
        return state

    def step(self, action):
        if action not in [0, 1, 2, 3]:
            raise ValueError('Unknown action {!r}!'.format(action))

        reward = self.movement_punishment
        done = False
        x, y = self.agent_pos

        # Go up
        if action == 0 and y < 27:
            y += 1

        # Go down
        elif action == 1 and y > 0:
            y -= 1

        # Go left
        elif action == 2 and x > 0:
            x -= 1

        # Go right
        elif action == 3 and x < 27:
            x += 1

        self.agent_pos = (x, y)

        # Check if the agent has reached the goal position
        if self.agent_pos in self.goal_positions:
            done = True
            reward = self.right_reward

        obs = self.agent_pos

        return obs, reward, done

def render(self, mode='human', close=False):
    # Render the environment to the screen. Prints the current position of the agent.

    if mode == 'human':
        grid = [['_' for _ in range(28)] for _ in range(28)]
        x, y = self.agent_pos
        
        # Set the 2x2 symbol at the position of the agent
        grid[x][y] = 'AA'
        grid[x+1][y] = 'AA'
        
        for row in grid:
            print(' '.join(row))
    else:
        raise ValueError('Unknown rendering mode {!r}!'.format(mode))

