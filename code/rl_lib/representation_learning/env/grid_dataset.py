import torch
from torch.utils.data import Dataset, DataLoader
import numpy as np
from representation_learning.env.grid import Grid
import matplotlib.pyplot as plt


class GridDataset(Dataset):
    def __init__(self, dataset_file):
        self.dataset = np.load(dataset_file)

    def __len__(self):
        return len(self.dataset)

    def __getitem__(self, idx):
        sample = self.dataset[idx]
        sample = torch.from_numpy(sample).float()
        return sample



def generate_dataset(env, num_samples, save_path):
    dataset = np.zeros((num_samples, 28, 28), dtype=int)

    for i in range(num_samples):
        obs = env.reset()
        done = False

        # Generate a random position for the agent
        agent_pos = (np.random.randint(0, 28), np.random.randint(0, 28))

        # Run the agent for a random number of steps
        num_steps = np.random.randint(1, 1001)
        for step in range(num_steps):
            valid_actions = [0, 1, 2, 3]  # valid actions for the current state
            action = np.random.choice(valid_actions)
            obs, _, done = env.step(action)

            # Update the agent position in the dataset
            agent_row, agent_col = agent_pos
            dataset[i, agent_row:agent_row+6, agent_col:agent_col+6] = 1

        # Save the final agent position in the dataset
        dataset[i][agent_pos[0]][agent_pos[1]] = 1


        plt.imsave(f"{save_path}_{i}.png", dataset[i], cmap='binary')

    np.save(save_path, dataset)
    print(dataset.shape)
    print(dataset[10])

    return dataset




if __name__ == '__main__':
    # Create Grid environment
    env = Grid()

    # Generate dataset
    num_samples = 200
    save_path = 'Dataset/new_grid_dataset.npy'
    generate_dataset(env, num_samples, save_path)

    # Load dataset from saved file and create GridDataset
    dataset = GridDataset(save_path)

    # Create DataLoader
    dataloader = DataLoader(dataset, batch_size=128, shuffle=True)

    # Print dataset shape
    print(dataset[0].shape)
