import os

path = '../exputils_rl_demo/experiments/Dataset'


def list_files(path):
    # Check if path exists
    if os.path.exists(path):
        # List files in the directory
        files = os.listdir(path)
        print("Files in the directory:")
        for file in files:
            print(file)
    else:
        print("Path does not exist")


# Call the function to list files in the directory
list_files(path)
