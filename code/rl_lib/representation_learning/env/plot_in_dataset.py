import numpy as np
import matplotlib.pyplot as plt

# Load the dataset
dataset = np.load('Dataset/new_grid_dataset.npy')

# Create a figure and axes object
fig, ax = plt.subplots()

# Loop through the dataset and plot each sample
for i in range(dataset.shape[0]):
    # Find the position of the 1 in the sample
    pos = np.where(dataset[i] == 1)
    
    # Plot the position as a scatter point on the same axes
    ax.scatter(pos[1], pos[0], c='b', alpha=0.5)

# Set the x and y limits to show the entire grid
ax.set_xlim(-2, 30)
ax.set_ylim(-2, 30)
plt.savefig('in_dataset.png')
print(dataset.shape)
print(dataset[5])

