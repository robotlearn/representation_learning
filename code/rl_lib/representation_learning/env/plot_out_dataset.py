import numpy as np
import matplotlib.pyplot as plt

# Load the dataset
dataset = np.load('output_data.npy')
dataset = dataset.reshape((200, 28, 28))


# Create a figure and axes object
fig, ax = plt.subplots()

# Loop through the dataset and plot the minimum value for each sample
for i in range(dataset.shape[0]):
    # Find the position of the minimum value in the sample
    min_pos = np.unravel_index(np.argmin(dataset[i]), dataset[i].shape)

    # Plot the minimum value as a scatter point on the same axes
    ax.scatter(min_pos[1], min_pos[0], c='b', alpha=0.5)

# Set the x and y limits to show the entire grid

ax.set_xlim(-2, 30)
ax.set_ylim(-2, 30)
plt.savefig('out_dataset.png')
print(dataset.shape)
print(dataset[5])
