import numpy as np
import exputils as eu
import torch
import exputils.data.logging as log
import representation_learning
from representation_learning.env.grid_dataset import GridDataset
import matplotlib.pyplot as plt
import os


def train(config=None, **kwargs):
    device = 'cpu'
    # device = 'cuda' if torch.cuda.is_available() else 'cpu'
    default_config = eu.AttrDict(
        seed=None,
        model=eu.AttrDict(
            cls=representation_learning.models.Variationalmodel,
            latent_dims=2
        ),

        optimizer=eu.AttrDict(
            cls=torch.optim.Adam,
            lr=0.001
        ),

        loss=lambda x, x_hat, vae: ((x - x_hat) ** 2).sum(),
        # ((x - x_hat) ** 2).sum() + vae.encoder.kl  # VAE loss

        data=None,
        epochs=10
    )
    config = eu.combine_dicts(kwargs, config, default_config)

    eu.misc.seed(config)

    model = eu.misc.create_object_from_config(config.model)
    model.to(device)
    data = config.data
    loss_func = config.loss
    latent_dims = config.model.latent_dims
    # Initialize the optimizer
    opt = eu.misc.create_object_from_config(
        config.optimizer, model.parameters())
    for epoch in range(config.epochs):
        total_loss = 0
        reconstructed_data = []  # initialize the list for storing reconstructed data
        # Loop through the data batches
        for x in data:
            x = x.to(device)  # Move the input data to the device
            opt.zero_grad()  # Zero the gradients
            x_hat = model(x)  # Forward pass
            # Compute the reconstruction loss
            loss = loss_func(x, x_hat, model)
            loss.backward()  # Backpropagation
            opt.step()  # Update the parameters
            total_loss += loss.item()

        print("Epoch {}: Loss = {:.4f}".format(epoch + 1, total_loss))

        log.add_value('epoch', epoch)
        log.add_value('total_loss', total_loss,
                      log_to_tb=True, tb_global_step=epoch)

        for x in config.data:
            x = x.to(device)
            z = model.encoder(x)
            # pass encoded data through decoder to get reconstructed data
            x_hat = model.decoder(z)
            reconstructed_data.append(x_hat.detach().cpu().numpy())

    reconstructed_data = np.concatenate(reconstructed_data, axis=0)
    # transpose to get (num_images, width, height, channels)
    reconstructed_data = np.transpose(reconstructed_data, (0, 2, 3, 1))
    np.save('output_data.npy', reconstructed_data)
    print("Output data saved as a {}x{}x{}x{} numpy array.".format(
        reconstructed_data.shape[0], reconstructed_data.shape[1], reconstructed_data.shape[2], reconstructed_data.shape[3]))

    print("Latent dimensions =", latent_dims)
    print("----------------------------------------")
    log.save()

    if not os.path.exists('data/output_data/image'):
        os.makedirs('data/output_data/image')

    for i in range(reconstructed_data.shape[0]):
        img_array = (reconstructed_data[i] * 255).astype('uint8')
        plt.imsave('data/output_data/image{}.png'.format(i),
                   img_array[:, :, 0], cmap='binary')


if __name__ == '__main__':

    my_config = eu.AttrDict(
        data=torch.utils.data.DataLoader(
            GridDataset('experiments/Dataset/new_grid_dataset.npy'),
            batch_size=128,
            shuffle=True,
        ),
        epochs=10
    )
