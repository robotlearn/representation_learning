import numpy as np
import exputils as eu
import torch
import representation_learning
import os
import torch.nn.functional as F
import torchvision.transforms as transforms
from torchvision import transforms
from torch import nn, optim
from torchvision.utils import save_image
from representation_learning.models.vae1 import VAE1, optimizer
from torch.utils.data import Dataset, DataLoader
import torchvision.transforms as transforms
from PIL import Image

device = 'cuda' if torch.cuda.is_available() else 'cpu'


class MyDataset(Dataset):
    data_path = 'Dataset/'

    def __init__(self, data_path, transform=None):
        self.data = []
        for i in range(200):
            img_path = os.path.join(data_path, f'new_grid_dataset.npy_{i}.png')
            img = Image.open(img_path).convert('L')  # Load image as grayscale
            label = i % 10  # Assign label based on the image index
            self.data.append((img, label))
        self.transform = transform

    def __len__(self):
        return len(self.data)

    def __getitem__(self, idx):
        x = self.data[idx][0]
        y = self.data[idx][1]

        if self.transform:
            x = self.transform(x)

        return x, y


train_data = MyDataset('Dataset/', transform=transforms.ToTensor())
train_loader = torch.utils.data.DataLoader(train_data,
                                           batch_size=128, shuffle=True)


def train1(config=None, **kwargs):
    train_data = MyDataset('Dataset/', transform=transforms.ToTensor())
    train_loader = torch.utils.data.DataLoader(train_data,
                                               batch_size=128, shuffle=True)
    test_data = MyDataset('Dataset/', transform=transforms.ToTensor())
    test_loader = torch.utils.data.DataLoader(test_data,
                                              batch_size=128, shuffle=False)

    default_config = eu.AttrDict(
        seed=1000,
        model=eu.AttrDict(
            cls=representation_learning.models.VAE1,
            latent_dims=4
        ),
        optimizer=eu.AttrDict(
            cls=torch.optim.Adam,
            lr=0.001
        ),
        loss=lambda recon_x, x, mu, logvar: F.binary_cross_entropy(
            recon_x, x.view(-1, 784), reduction='sum') + 0.5 * torch.sum(mu.pow(2) + logvar.exp() - logvar - 1),
        data=eu.AttrDict(
            train_loader=train_loader
        ),
        epoch=400
    )
    config = eu.combine_dicts(kwargs, config, default_config)

    eu.misc.seed(config)
    data = config.data
    loss_func = config.loss
    # Initialize the optimizer
    opt = eu.misc.create_object_from_config(
        config.optimizer, model.parameters())
    model.train()
    for epoch in range(config.epoch):
        train_loss = 0
        for batch_idx, (data, label) in enumerate(train_loader):
            opt.zero_grad()
            data = data.to(device)
            recon_data, mu, logvar = model(data)
            loss = loss_func(recon_data, data, mu, logvar)
            loss.backward()
            train_loss += loss.item()

            opt.step()
        test(model, loss_func, epoch, test_loader)

        print('----- Epoch: {} Average loss: {:.4f}'.format(
            epoch, train_loss / len(train_loader.dataset)
        ))
    print('\033[92m' + 'latent dimensions = ' +
          str(default_config.model.latent_dims) + '\033[0m')


def test(model, loss_func, epoch, test_loader):
    model.eval()
    test_loss = 0
    with torch.no_grad():
        for batch_idx, (data, label) in enumerate(test_loader):
            data = data.to(device)
            recon_data, mu, logvar = model(data)
            cur_loss = loss_func(recon_data, data, mu, logvar).item()
            test_loss += cur_loss
            if batch_idx == 0:
                # saves 8 samples of the first batch as an image file to compare input images and reconstructed images
                num_samples = min(128, 8)
                comparison = torch.cat(
                    [data[:num_samples], recon_data.view(128, 1, 28, 28)[:num_samples]]).cpu()
                save_generated_img(
                    comparison, 'reconstruction', epoch, num_samples)
                sample_from_model(epoch)
                plot_along_axis(epoch)

    test_loss /= len(test_loader.dataset)
    print('-----  Test loss: {:.1f}'.format(test_loss))


def save_generated_img(image, name, epoch, nrow=8):
    if not os.path.exists('results'):
        os.makedirs('results')

    if epoch % 50 == 0:
        save_path = 'results/'+name+'_'+str(epoch)+'.png'
        save_image(image, save_path, nrow=nrow)


def sample_from_model(epoch):
    with torch.no_grad():
        # p(z) = N(0,I), this distribution is used when calculating KLD. So we can sample z from N(0,I)
        sample = torch.randn(64, 2).to(device)
        sample = model.decode(sample).cpu().view(64, 1, 28, 28)
        # print(epoch)
        save_generated_img(sample, 'sample', epoch)


def plot_along_axis(epoch):
    z1 = torch.arange(-2, 2, 0.2).to(device)
    z2 = torch.arange(-2, 2, 0.2).to(device)
    num_z1 = z1.shape[0]
    num_z2 = z2.shape[0]
    num_z = num_z1 * num_z2

    sample = torch.zeros(num_z, 2).to(device)

    for i in range(num_z1):
        for j in range(num_z2):
            idx = i * num_z2 + j
            sample[idx][0] = z1[i]
            sample[idx][1] = z2[j]
    # print('epoch is',epoch)
    sample = model.decode(sample).cpu().view(num_z, 1, 28, 28)
    save_generated_img(sample, 'plot_along_z1_and_z2_axis', epoch, num_z1)


def loss_function(recon_x, x, mu, logvar):
    BCE = F.binary_cross_entropy(recon_x, x.view(-1, 784), reduction='sum')
    KLD = 0.5 * torch.sum(mu.pow(2) + logvar.exp() - logvar - 1)

    return BCE + KLD


if __name__ == '__main__':
    model = VAE1().to(device)
    train1()