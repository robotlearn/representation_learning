import exputils as eu
import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.utils
import torch.distributions
import numpy as np
import matplotlib.pyplot as plt
import exputils.data.logging as log


# Set device
device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

# Define Encoder class

class Encoder(nn.Module):
    @staticmethod
    def default_config():
        return eu.AttrDict(latent_dims=5)

    def __init__(self, config=None, **kwargs):
        super(Encoder, self).__init__()
        self.config = eu.combine_dicts(
            kwargs, config, Encoder.default_config())
        # Define first linear layer with 784 input features and 512 output features
        self.linear1 = nn.Linear(784, 512)
        # Define second linear layer with 512 input features and the given number of latent dimensions output
        self.linear2 = nn.Linear(512, self.config.latent_dims)

    # Define the forward method of the Encoder class
    def forward(self, x):
        # Flatten the input tensor along the second dimension
        x = torch.flatten(x, start_dim=1)
        # Pass the flattened tensor through the first linear layer and apply ReLU activation function
        x = F.relu(self.linear1(x))
        # Pass the output tensor through the second linear layer and return it
        return self.linear2(x)


# Define Decoder class
class Decoder(nn.Module):
    @staticmethod
    def default_config():
        return eu.AttrDict(latent_dims=5)

    def __init__(self, config=None, **kwargs):
        super(Decoder, self).__init__()
        self.config = eu.combine_dicts(
            kwargs, config, Decoder.default_config())
        # Define first linear layer with the given number of latent dimensions input and 512 output features
        self.linear1 = nn.Linear(self.config.latent_dims, 512)
        # Define second linear layer with 512 input features and 784 output features
        self.linear2 = nn.Linear(512, 784)

    # Define the forward method of the Decoder class
    def forward(self, z):
        # Pass the input tensor through the first linear layer and apply ReLU activation function
        z = F.relu(self.linear1(z))
        # Pass the output tensor through the second linear layer and apply sigmoid activation function
        z = torch.sigmoid(self.linear2(z))
        # Reshape the output tensor to have shape (-1, 1, 28, 28)
        return z.reshape((-1, 1, 28, 28))
