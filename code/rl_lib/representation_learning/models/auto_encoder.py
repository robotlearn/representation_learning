# Import necessary libraries
import exputils as eu
import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.utils
import torch.distributions
import numpy as np
import exputils.data.logging as log
from .architicture import Encoder, Decoder


# Set device
device = torch.device("cuda" if torch.cuda.is_available() else "cpu")


class Autoencoder(nn.Module):
    # Define a static method that returns a dictionary with default configuration parameters
    @staticmethod
    def default_config():
        return eu.AttrDict(latent_dims=5)

    def __init__(self, config=None, **kwargs):
        super(model, self).__init__()
        self.config = eu.combine_dicts(
            kwargs, config, model.default_config())
        # Initialize the encoder module with the number of latent dimensions
        self.encoder = Encoder(latent_dims = self.config.latent_dims)
        # Initialize the decoder module with the number of latent dimensions
        self.decoder = Decoder(latent_dims = self.config.latent_dims)

    # Define the forward method for the model class
    def forward(self, x):
        # Encode the input x to get a latent representation z
        z = self.encoder(x)
        # Decode the latent representation z to get the output x_hat
        return self.decoder(z)