import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
import exputils as eu

class VAE1(nn.Module):
    @staticmethod
    def default_config():
        return eu.AttrDict(latent_dims=2)

    def __init__(self, config=None, **kwargs):
        super(VAE1, self).__init__()
        self.config = eu.combine_dicts(
            kwargs, config, VAE1.default_config()
        )
        self.fc1 = nn.Linear(784, 500)
        self.fc21 = nn.Linear(500, self.config.latent_dims)
        self.fc22 = nn.Linear(500, self.config.latent_dims)
        self.fc3 = nn.Linear(self.config.latent_dims, 500)
        self.fc4 = nn.Linear(500, 784)

    def encode(self, x):
        h1 = F.relu(self.fc1(x))
        mu = self.fc21(h1)
        logvar = self.fc22(h1)
        # print("h1 shape:", h1.shape)
        return mu, logvar

    def reparamiterize(self, mu, logvar):
        std = torch.exp(0.5*logvar)
        eps = torch.rand_like(std)
        return mu + eps*std

    def decode(self, z):
        h3 = F.relu(self.fc3(z))
        return torch.sigmoid(self.fc4(h3))

    def forward(self, x):
        x = x.view(-1, 784)
        mu, logvar = self.encode(x)
        z = self.reparamiterize(mu, logvar)
        h3 = F.relu(self.fc3(z.view(-1, self.config.latent_dims)))
        # print("h3 shape:", h3.shape)
        return torch.sigmoid(self.fc4(h3)), mu, logvar

device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
model = VAE1().to(device)
optimizer = optim.Adam(model.parameters(), lr=0.001)