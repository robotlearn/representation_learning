import exputils as eu
import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.utils
import torch.distributions
import torchvision
import numpy as np
import matplotlib.pyplot as plt
import exputils.data.logging as log
from .architicture import Encoder, Decoder

# Define a VariationalEncoder class that inherits from the nn.Module class


class VariationalEncoder(nn.Module):
    @staticmethod
    def default_config():
        return eu.AttrDict(latent_dims=2)

    def __init__(self, config=None, **kwargs):
        super(VariationalEncoder, self).__init__()
        self.config = eu.combine_dicts(
            kwargs, config, VariationalEncoder.default_config()
        )

        # Define a linear layer with input size of 784 and output size of 512
        self.linear1 = nn.Linear(784, 512)

        # Define a linear layer with input size of 512 and output size of latent_dims
        self.linear2 = nn.Linear(512, self.config.latent_dims)

        # Define a linear layer with input size of 512 and output size of latent_dims
        self.linear3 = nn.Linear(512, self.config.latent_dims)

        # Define a normal distribution with a mean of 0 and standard deviation of 1
        self.N = torch.distributions.Normal(0, 1)

        # Initialize the value of the KL divergence loss to 0
        self.kl = 0

    # Define the forward method
    def forward(self, x):

        # Flatten the input tensor x along the second dimension
        x = torch.flatten(x, start_dim=1)

        # Apply the ReLU activation function to the output of the first linear layer
        x = F.relu(self.linear1(x))

        # Compute the mean of the latent variable z
        mu = self.linear2(x)

        # Compute the standard deviation of the latent variable z
        sigma = torch.exp(self.linear3(x))

        # Sample from the normal distribution to generate the latent variable z
        z = mu + sigma * self.N.sample(mu.shape)
        # .to("cuda:0")

        # Compute the KL divergence loss
        self.kl = (sigma**2 + mu**2 - torch.log(sigma) - 1 / 2).sum()

        # Return the latent variable z
        return z


# Define a Variationalmodel class that inherits from the nn.Module class
class Variationalmodel(nn.Module):
    @staticmethod
    def default_config():
        return eu.AttrDict(latent_dims=2)

    def __init__(self, config=None, **kwargs):
        super(Variationalmodel, self).__init__()
        self.config = eu.combine_dicts(
            kwargs, config, Variationalmodel.default_config()
        )

        # Define an instance of the VariationalEncoder class
        self.encoder = VariationalEncoder(latent_dims=self.config.latent_dims)

        # Define an instance of the Decoder class
        self.decoder = Decoder(latent_dims=self.config.latent_dims)

    # Define the forward method
    def forward(self, x):

        # Encode the input tensor x using the VariationalEncoder
        z = self.encoder(x)
        # .to("cuda:0")
        return self.decoder(z)