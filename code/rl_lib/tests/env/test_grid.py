import gym
from representation_learning.envs.grid import Grid

def test_grid_env():
    env = Grid()
    obs = env.reset()
    assert env.observation_space.contains(obs)
    assert env.action_space.contains(0)
    assert env.action_space.contains(1)
    assert env.action_space.contains(2)
    assert env.action_space.contains(3)
    done = False
    while not done:
        obs, reward, done, info = env.step(env.action_space.sample())
        assert env.observation_space.contains(obs)
        assert isinstance(reward, float)
        assert isinstance(done, bool)
        assert isinstance(info, dict)