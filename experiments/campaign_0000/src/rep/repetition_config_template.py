import numpy as np
import exputils as eu
import representation_learning
import torch
import torchvision
from representation_learning.env.grid_dataset import GridDataset


config = eu.AttrDict(
    # random seed for the repetition
    seed = 3487 + 0,

    model=eu.AttrDict(
        cls=representation_learning.models.<model>, # Autoencoder, VariationalAutoencoder
        latent_dims=<n_latent_dims>,
    ),

    optimizer = eu.AttrDict(
        cls=torch.optim.<optimizer>, # Adam, SGD 
        lr=<lr>,
    ), 


    loss = lambda x, x_hat, vae: ((x - x_hat) ** 2).sum() + vae.encoder.kl,
    # ((x - x_hat) ** 2).sum() + vae.encoder.kl  # VAE loss

    data = torch.utils.data.DataLoader(
            GridDataset('../../../../Dataset/new_grid_dataset.npy'),
            batch_size=<batch_size>,
            shuffle=True,
    ),

    epochs=<n_epochs>,
)

#     data = torch.utils.data.DataLoader(
#             torchvision.datasets.MNIST(
#                 '../../../Dataset',
#                 transform=torchvision.transforms.ToTensor(),
#                 download=True,
#             ),
#             batch_size=<batch_size>,
#             shuffle=True,
#     ),

#     epochs=<n_epochs>,

# )


# import os

# path = '../../../../Dataset/grid_dataset.npy'

# def list_files(path):
#     # Check if path exists
#     if os.path.exists(path):
#         # List files in the directory
#         files = os.listdir(path)
#         print("Files in the directory:")
#         for file in files:
#             print(file)
#     else:
#         print("Path does not exist")

# # Call the function to list files in the directory
# list_files(path)






if config.model.cls == representation_learning.models.VariationalEncoder:
    config.loss = lambda x, x_hat, vae: ((x - x_hat) ** 2).sum() + vae.encoder.kl  # VAE loss