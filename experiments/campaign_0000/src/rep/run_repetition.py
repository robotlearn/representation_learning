#!/usr/bin/env python3
import representation_learning
from repetition_config import config

# run experiment
representation_learning.exp.train(config=config)

