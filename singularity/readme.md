# Introduction

To run the code on a cluster, we are using singularity to run the experiments in a virtual machine.
This document describes how to create and configure the singularity container.

# Installation of Singularity

See https://team.inria.fr/perception/private/how-to-use-singularity-a-memo/ for more information.


# Creation of the Singularity Image

To create the image, we first create a *requirement.txt* file that contains all required python libraries for the project.
Copy for this all requirements from your setup.cfg files into the *requirement.txt* file. 

Then, the singularity image file can be created using the definition file *csfr.def*:

`sudo singularity build csfr.sif csfr.def`

(If you are on a Inria workstation then you might want to use different temporary folder for Singularity, as the image creation needs space. See https://team.inria.fr/perception/private/how-to-use-singularity-a-memo/ for more information.)
